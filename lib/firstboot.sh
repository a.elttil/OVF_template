#!/bin/bash
############################################################
# <?xml version="1.0" encoding="UTF-8"?>
# <Environment
#      xmlns="http://schemas.dmtf.org/ovf/environment/1"
#      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
#      xmlns:oe="http://schemas.dmtf.org/ovf/environment/1"
#      xmlns:ve="http://www.vmware.com/schema/ovfenv"
#      oe:id=""
#      ve:vCenterId="vm-101">
#    <PlatformSection>
#       <Kind>VMware ESXi</Kind>
#       <Version>6.5.0</Version>
#       <Vendor>VMware, Inc.</Vendor>
#       <Locale>en</Locale>
#    </PlatformSection>
#    <PropertySection>
#          <Property oe:key="dns0" oe:value="8.8.8.8"/>
#          <Property oe:key="gateway" oe:value="10.1.1.1"/>
#          <Property oe:key="hostname" oe:value="alt-001-test"/>
#          <Property oe:key="ip0" oe:value="10.0.0.1"/>
#          <Property oe:key="ip_static" oe:value="True"/>
#          <Property oe:key="netmask0" oe:value="255.255.255.0"/>
#          <Property oe:key="password" oe:value="Passw0rd!2017"/>
#    </PropertySection>
#    <ve:EthernetAdapterSection>
#       <ve:Adapter ve:mac="00:50:56:a1:34:22" ve:network="CT-LAB-SRV" ve:unitNumber="7"/>
#    </ve:EthernetAdapterSection>
# </Environment>
############################################################

## Init des variables et env
DEFAULT_HOSTNAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1)
DEFAULT_PASSWORD="Passw0rd!2017"
DEFAULT_NETWORKCONFIG="/etc/network/interfaces"
DEFAULT_HOSTS="/etc/hosts"

TMPXML='/root/OVF_template/ovf_env.xml'
LOGFILE='/root/OVF_template/setup.log'

## Retrieve guestinfo params
vmtoolsd --cmd "info-get guestinfo.ovfenv" > $TMPXML

### START
echo "---- STARTING fisrt setup ---" >> $LOGFILE
echo "date: " $(date +"%m.%d.%Y %T ") >> $LOGFILE


### SET HOSTNAME
HOSTNAME=$(cat $TMPXML| grep -e hostname |sed -n -e '/value\=/ s/.*\=\" *//p'|sed 's/\"\/>//')
if [ ! $HOSTNAME ]; then
    echo 'WARN - HOSTNAME params is empty!' >> $LOGFILE
    $HOSTNAME=$DEFAULT_HOSTNAME
fi

echo "SET HOSTNAME : " $HOSTNAME >> $LOGFILE
hostnamectl set-hostname $HOSTNAME --static

cat > $DEFAULT_HOSTS <<EOF
127.0.0.1	localhost
127.0.1.1	$HOSTNAME

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF


### SET PASSWORD
echo "SET PASSWORD : " $HOSTNAME >> $LOGFILE
PASSWORD=$(cat $TMPXML| grep -e password |sed -n -e '/value\=/ s/.*\=\" *//p'|sed 's/\"\/>//')
if [ ! $PASSWORD ]; then
    echo 'password params is empty. Save default password' >> $LOGFILE
    $PASSWORD=$DEFAULT_PASSWORD
fi
echo "SET PASSWORD : " $PASSWORD >> $LOGFILE
echo "user:$PASSWORD" | chpasswd
echo "root:$PASSWORD" | chpasswd


### SET IP ADDRESS
echo "SET IP ADDRESS : " $HOSTNAME >> $LOGFILE
IPSTATIC=`cat $TMPXML| grep -e ip_static |sed -n -e '/value\=/ s/.*\=\" *//p'|sed 's/\"\/>//'`
if [ ! $IPSTATIC ]; then
    echo 'param ip_static is empty' >> $LOGFILE

elif [ $IPSTATIC == 'True' ]; then
    echo 'IP proto is static' >> $LOGFILE
    IP=`cat $TMPXML| grep -e ip0 |sed -n -e '/value\=/ s/.*\=\" *//p'|sed 's/\"\/>//'`
    NETMASK=`cat $TMPXML| grep -e netmask0 |sed -n -e '/value\=/ s/.*\=\" *//p'|sed 's/\"\/>//'`
    GW=`cat $TMPXML| grep -e gateway |sed -n -e '/value\=/ s/.*\=\" *//p'|sed 's/\"\/>//'`
    DNS0=`cat $TMPXML| grep -e dns0 |sed -n -e '/value\=/ s/.*\=\" *//p'|sed 's/\"\/>//'`

cat > $DEFAULT_NETWORKCONFIG <<EOF
    # This file describes the network interfaces available on your system
    # and how to activate them. For more information, see interfaces(5).

    source /etc/network/interfaces.d/*
    auto lo
    iface lo inet loopback

    # The primary network interface
    auto ens160
    iface ens160 inet static
    address $IP
    netmask $NETMASK
    gateway $GW
    dns-nameservers $DNS0
EOF

else
    echo 'IP proto is DHCP' >> $LOGFILE
fi

echo "---- END first setup ---" >> $LOGFILE
echo "date: " $(date +"%m.%d.%Y %T ") >> $LOGFILE

# Update fc conf and rm file
update-rc.d firstboot -f remove
rm /etc/init.d/firstboot
systemctl daemon-reload

# Reboot into the new kernel
/sbin/reboot

exit 0
