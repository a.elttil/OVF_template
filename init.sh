#!/bin/sh
############################################################
######  INIT DU SCRIPT firstboot
############################################################

echo "START INSTALL THE FIRSTBOOT SERVICE"
echo "CP firstboot"
cp lib/firstboot.sh /etc/init.d/firstboot


############################################################
######  INSTALL THE FIRSTBOOT SERVICE
############################################################
echo "INSTALL THE FIRSTBOOT SERVICE"
chmod a+x /etc/init.d/firstboot
update-rc.d firstboot defaults
systemctl daemon-reload
echo "THE FIRSTBOOT SCRIPT IS INSTALLED"
